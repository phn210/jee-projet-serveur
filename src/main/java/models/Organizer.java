package models;

import jakarta.persistence.*;

@Entity
@Table(name = "organizers")
public class Organizer {
    @Id
    @OneToOne
    @JoinColumn(name = "id", referencedColumnName = "user_id")
    private User organizer;
    public Organizer() {
    }

    public User getOrganizer() {
        return organizer;
    }

    public void setOrganizer(User organizer) {
        this.organizer = organizer;
    }

}
